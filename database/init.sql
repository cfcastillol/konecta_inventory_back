--
-- PostgreSQL database dump
--

-- Dumped from database version 14.5
-- Dumped by pg_dump version 14.5

-- Started on 2022-09-30 10:43:36

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 5 (class 2615 OID 16394)
-- Name: konecta_inventory; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA konecta_inventory;


ALTER SCHEMA konecta_inventory OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 212 (class 1259 OID 16417)
-- Name: category; Type: TABLE; Schema: konecta_inventory; Owner: postgres
--

CREATE TABLE konecta_inventory.category (
    id integer NOT NULL,
    category character varying(255) NOT NULL,
    create_at timestamp without time zone DEFAULT now(),
    active smallint DEFAULT 1 NOT NULL,
    update_at timestamp without time zone DEFAULT now()
);


ALTER TABLE konecta_inventory.category OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 16416)
-- Name: category_id_seq; Type: SEQUENCE; Schema: konecta_inventory; Owner: postgres
--

CREATE SEQUENCE konecta_inventory.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE konecta_inventory.category_id_seq OWNER TO postgres;

--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 211
-- Name: category_id_seq; Type: SEQUENCE OWNED BY; Schema: konecta_inventory; Owner: postgres
--

ALTER SEQUENCE konecta_inventory.category_id_seq OWNED BY konecta_inventory.category.id;


--
-- TOC entry 214 (class 1259 OID 16424)
-- Name: product; Type: TABLE; Schema: konecta_inventory; Owner: postgres
--

CREATE TABLE konecta_inventory.product (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    category_id integer NOT NULL,
    reference character varying(255) NOT NULL,
    price numeric NOT NULL,
    stock bigint NOT NULL,
    create_at timestamp without time zone DEFAULT now(),
    active smallint DEFAULT 1,
    update_at timestamp without time zone DEFAULT now()
);


ALTER TABLE konecta_inventory.product OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16423)
-- Name: product_id_seq; Type: SEQUENCE; Schema: konecta_inventory; Owner: postgres
--

CREATE SEQUENCE konecta_inventory.product_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE konecta_inventory.product_id_seq OWNER TO postgres;

--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 213
-- Name: product_id_seq; Type: SEQUENCE OWNED BY; Schema: konecta_inventory; Owner: postgres
--

ALTER SEQUENCE konecta_inventory.product_id_seq OWNED BY konecta_inventory.product.id;


--
-- TOC entry 218 (class 1259 OID 16529)
-- Name: product_sale; Type: TABLE; Schema: konecta_inventory; Owner: postgres
--

CREATE TABLE konecta_inventory.product_sale (
    id integer NOT NULL,
    product_id integer NOT NULL,
    sale_id integer NOT NULL,
    product_qty bigint NOT NULL,
    product_name character varying(100) NOT NULL,
    product_reference character varying(100) NOT NULL,
    product_price numeric NOT NULL,
    product_category character varying(100) NOT NULL,
    product_stock bigint NOT NULL,
    create_at timestamp without time zone DEFAULT now(),
    updated_at timestamp without time zone DEFAULT now()
);


ALTER TABLE konecta_inventory.product_sale OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 16528)
-- Name: product_sale_id_seq; Type: SEQUENCE; Schema: konecta_inventory; Owner: postgres
--

CREATE SEQUENCE konecta_inventory.product_sale_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE konecta_inventory.product_sale_id_seq OWNER TO postgres;

--
-- TOC entry 3360 (class 0 OID 0)
-- Dependencies: 217
-- Name: product_sale_id_seq; Type: SEQUENCE OWNED BY; Schema: konecta_inventory; Owner: postgres
--

ALTER SEQUENCE konecta_inventory.product_sale_id_seq OWNED BY konecta_inventory.product_sale.id;


--
-- TOC entry 216 (class 1259 OID 16440)
-- Name: sale; Type: TABLE; Schema: konecta_inventory; Owner: postgres
--

CREATE TABLE konecta_inventory.sale (
    id integer NOT NULL,
    total_sale numeric NOT NULL,
    create_at timestamp without time zone DEFAULT now(),
    update_at timestamp without time zone DEFAULT now()
);


ALTER TABLE konecta_inventory.sale OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16439)
-- Name: sale_id_seq; Type: SEQUENCE; Schema: konecta_inventory; Owner: postgres
--

CREATE SEQUENCE konecta_inventory.sale_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE konecta_inventory.sale_id_seq OWNER TO postgres;

--
-- TOC entry 3361 (class 0 OID 0)
-- Dependencies: 215
-- Name: sale_id_seq; Type: SEQUENCE OWNED BY; Schema: konecta_inventory; Owner: postgres
--

ALTER SEQUENCE konecta_inventory.sale_id_seq OWNED BY konecta_inventory.sale.id;


--
-- TOC entry 3181 (class 2604 OID 16420)
-- Name: category id; Type: DEFAULT; Schema: konecta_inventory; Owner: postgres
--

ALTER TABLE ONLY konecta_inventory.category ALTER COLUMN id SET DEFAULT nextval('konecta_inventory.category_id_seq'::regclass);


--
-- TOC entry 3185 (class 2604 OID 16427)
-- Name: product id; Type: DEFAULT; Schema: konecta_inventory; Owner: postgres
--

ALTER TABLE ONLY konecta_inventory.product ALTER COLUMN id SET DEFAULT nextval('konecta_inventory.product_id_seq'::regclass);


--
-- TOC entry 3192 (class 2604 OID 16532)
-- Name: product_sale id; Type: DEFAULT; Schema: konecta_inventory; Owner: postgres
--

ALTER TABLE ONLY konecta_inventory.product_sale ALTER COLUMN id SET DEFAULT nextval('konecta_inventory.product_sale_id_seq'::regclass);


--
-- TOC entry 3189 (class 2604 OID 16443)
-- Name: sale id; Type: DEFAULT; Schema: konecta_inventory; Owner: postgres
--

ALTER TABLE ONLY konecta_inventory.sale ALTER COLUMN id SET DEFAULT nextval('konecta_inventory.sale_id_seq'::regclass);


--
-- TOC entry 3346 (class 0 OID 16417)
-- Dependencies: 212
-- Data for Name: category; Type: TABLE DATA; Schema: konecta_inventory; Owner: postgres
--

COPY konecta_inventory.category (id, category, create_at, active, update_at) FROM stdin;
6	werwerwer	2022-09-28 00:00:00	1	2022-09-29 00:00:00
7	werwerwer12121	2022-09-28 00:00:00	1	2022-09-29 00:00:00
8	60000	2022-09-29 00:00:00	1	2022-09-29 00:00:00
\.


--
-- TOC entry 3348 (class 0 OID 16424)
-- Dependencies: 214
-- Data for Name: product; Type: TABLE DATA; Schema: konecta_inventory; Owner: postgres
--

COPY konecta_inventory.product (id, name, category_id, reference, price, stock, create_at, active, update_at) FROM stdin;
6	gallina	7	aaaaaaa	40000	122	2022-09-28 00:00:00	0	2022-09-29 00:00:00
7	poliito	6	werwerw	40000	2216	2022-09-29 00:00:00	0	2022-09-29 00:00:00
8	pavo	8	3P	333	200	2022-09-29 16:47:28.200819	0	2022-09-29 16:47:28.200819
\.


--
-- TOC entry 3352 (class 0 OID 16529)
-- Dependencies: 218
-- Data for Name: product_sale; Type: TABLE DATA; Schema: konecta_inventory; Owner: postgres
--

COPY konecta_inventory.product_sale (id, product_id, sale_id, product_qty, product_name, product_reference, product_price, product_category, product_stock, create_at, updated_at) FROM stdin;
1	6	5	1	KCRMBANCOLOMBIA	123	30000	werwerwer12121	123	2022-09-29 00:00:00	2022-09-29 00:00:00
2	7	5	1	FrontBancolombiaSI	werwerw	30000	werwerwer	123	2022-09-29 00:00:00	2022-09-29 00:00:00
3	6	6	1	KCRMBANCOLOMBIA	123	30000	werwerwer12121	123	2022-09-29 00:00:00	2022-09-29 00:00:00
4	7	6	1	FrontBancolombiaSI	werwerw	30000	werwerwer	123	2022-09-29 00:00:00	2022-09-29 00:00:00
5	6	7	12	KCRMBANCOLOMBIA	123	30000	werwerwer12121	123	2022-09-29 00:00:00	2022-09-29 00:00:00
6	7	7	1	FrontBancolombiaSI	werwerw	30000	werwerwer	123	2022-09-29 00:00:00	2022-09-29 00:00:00
7	6	9	1	KCRMBANCOLOMBIA	123	30000	werwerwer12121	123	2022-09-29 00:00:00	2022-09-29 00:00:00
8	7	9	1	FrontBancolombiaSI	werwerw	30000	werwerwer	123	2022-09-29 00:00:00	2022-09-29 00:00:00
\.


--
-- TOC entry 3350 (class 0 OID 16440)
-- Dependencies: 216
-- Data for Name: sale; Type: TABLE DATA; Schema: konecta_inventory; Owner: postgres
--

COPY konecta_inventory.sale (id, total_sale, create_at, update_at) FROM stdin;
1	60000	2022-09-29 00:00:00	2022-09-29 00:00:00
2	60000	2022-09-29 00:00:00	2022-09-29 00:00:00
3	60000	2022-09-29 00:00:00	2022-09-29 00:00:00
4	60000	2022-09-29 00:00:00	2022-09-29 00:00:00
5	60000	2022-09-29 00:00:00	2022-09-29 00:00:00
6	60000	2022-09-29 00:00:00	2022-09-29 00:00:00
7	390000	2022-09-29 00:00:00	2022-09-29 00:00:00
8	60000	2022-09-29 00:00:00	2022-09-29 00:00:00
9	60000	2022-09-29 00:00:00	2022-09-29 00:00:00
\.


--
-- TOC entry 3362 (class 0 OID 0)
-- Dependencies: 211
-- Name: category_id_seq; Type: SEQUENCE SET; Schema: konecta_inventory; Owner: postgres
--

SELECT pg_catalog.setval('konecta_inventory.category_id_seq', 8, true);


--
-- TOC entry 3363 (class 0 OID 0)
-- Dependencies: 213
-- Name: product_id_seq; Type: SEQUENCE SET; Schema: konecta_inventory; Owner: postgres
--

SELECT pg_catalog.setval('konecta_inventory.product_id_seq', 8, true);


--
-- TOC entry 3364 (class 0 OID 0)
-- Dependencies: 217
-- Name: product_sale_id_seq; Type: SEQUENCE SET; Schema: konecta_inventory; Owner: postgres
--

SELECT pg_catalog.setval('konecta_inventory.product_sale_id_seq', 8, true);


--
-- TOC entry 3365 (class 0 OID 0)
-- Dependencies: 215
-- Name: sale_id_seq; Type: SEQUENCE SET; Schema: konecta_inventory; Owner: postgres
--

SELECT pg_catalog.setval('konecta_inventory.sale_id_seq', 9, true);


--
-- TOC entry 3196 (class 2606 OID 16433)
-- Name: category category_pk; Type: CONSTRAINT; Schema: konecta_inventory; Owner: postgres
--

ALTER TABLE ONLY konecta_inventory.category
    ADD CONSTRAINT category_pk PRIMARY KEY (id);


--
-- TOC entry 3198 (class 2606 OID 16431)
-- Name: product product_pkey; Type: CONSTRAINT; Schema: konecta_inventory; Owner: postgres
--

ALTER TABLE ONLY konecta_inventory.product
    ADD CONSTRAINT product_pkey PRIMARY KEY (id);


--
-- TOC entry 3202 (class 2606 OID 16538)
-- Name: product_sale product_sale_pkey; Type: CONSTRAINT; Schema: konecta_inventory; Owner: postgres
--

ALTER TABLE ONLY konecta_inventory.product_sale
    ADD CONSTRAINT product_sale_pkey PRIMARY KEY (id);


--
-- TOC entry 3200 (class 2606 OID 16447)
-- Name: sale sale_pkey; Type: CONSTRAINT; Schema: konecta_inventory; Owner: postgres
--

ALTER TABLE ONLY konecta_inventory.sale
    ADD CONSTRAINT sale_pkey PRIMARY KEY (id);


--
-- TOC entry 3203 (class 2606 OID 16434)
-- Name: product product_fk; Type: FK CONSTRAINT; Schema: konecta_inventory; Owner: postgres
--

ALTER TABLE ONLY konecta_inventory.product
    ADD CONSTRAINT product_fk FOREIGN KEY (category_id) REFERENCES konecta_inventory.category(id);


--
-- TOC entry 3204 (class 2606 OID 16539)
-- Name: product_sale product_sale_product_fk; Type: FK CONSTRAINT; Schema: konecta_inventory; Owner: postgres
--

ALTER TABLE ONLY konecta_inventory.product_sale
    ADD CONSTRAINT product_sale_product_fk FOREIGN KEY (product_id) REFERENCES konecta_inventory.product(id);


--
-- TOC entry 3205 (class 2606 OID 16544)
-- Name: product_sale product_sale_sale_fk; Type: FK CONSTRAINT; Schema: konecta_inventory; Owner: postgres
--

ALTER TABLE ONLY konecta_inventory.product_sale
    ADD CONSTRAINT product_sale_sale_fk FOREIGN KEY (sale_id) REFERENCES konecta_inventory.sale(id);


-- Completed on 2022-09-30 10:43:36

--
-- PostgreSQL database dump complete
--


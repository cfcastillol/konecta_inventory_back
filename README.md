### Installation

This project a *Web Backend Application*.

First, clone the repo:

```bash
git clone https://gitlab.com/cfcastillol/konecta_inventory_back.git
```

to run the backend you can use docker:

```
cd konecta_inventory_back
docker-compose up
```

Sql query Max Stock
```bash
SELECT p.id as ID, p.name AS Producto, max(p.stock) as Stock FROM konecta_inventory.product p WHERE p.active = 1 group by p.id ;
```

Sql query SUM product sale
```bash
SELECT product_id AS producto, product_name as Nombre, SUM(product_qty) AS Total_Vendidos from konecta_inventory.product_sale GROUP BY product_id, product_name ORDER BY Total_Vendidos DESC;
```

const { Router } = require("express");
const {
  createProduct,
  getAllProduct,
  getProduct,
  updateProduct,
  deleteProduct,
} = require("../controllers/product.controller");

const router = Router();

// create a product
router.post("/product", createProduct);

router.get("/product", getAllProduct);

router.get("/product/:id", getProduct);

router.put("/product/:id", updateProduct);

router.post("/product/delete", deleteProduct);

module.exports = router;
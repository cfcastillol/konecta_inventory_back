const { Router } = require("express");
const { createSale } = require("../controllers/sale.controller");

const router = Router();

// create a category
router.post("/sale", createSale);

module.exports = router;
const { Router } = require("express");
const {
  createCategory,
  getAllCategory,
  getCategory,
  updateCategory,
  deleteCategory,
} = require("../controllers/category.controller");

const router = Router();

// create a category
router.post("/category", createCategory);

router.get("/category", getAllCategory);

router.get("/category/:id", getCategory);

router.put("/category/:id", updateCategory);

router.delete("/category/:id", deleteCategory);

module.exports = router;
const pool = require("../db");

const createProduct = async (req, res, next) => {
  try {
    const { name, category_id, reference, price, stock} = req.body;

    const newProduct = await pool.query(
      "INSERT INTO konecta_inventory.product (name,category_id,reference,price,stock) VALUES($1,$2,$3,$4,$5) RETURNING *",
      [name, category_id, reference, price, stock]
    );
    res.json(newProduct.rows[0]);
  } catch (error) {
    next(error);
  }
};

const getAllProduct = async (req, res, next) => {
  try {
    const allProducts = await pool.query(
      "SELECT p.*, cat.category FROM konecta_inventory.product as p, konecta_inventory.category as cat where p.active = 1 and p.category_id = cat.id "
    );
    res.json(allProducts.rows);
  } catch (error) {
    next(error);
  }
};

const getProduct = async (req, res, next) => {
  try {
    const { id } = req.params;
    const result = await pool.query(
      "SELECT * FROM konecta_inventory.product WHERE id = $1",
      [id]
    );

    if (result.rows.length === 0)
      return res.status(404).json({ message: "Product not found" });

    res.json(result.rows[0]);
  } catch (error) {
    next(error);
  }
};

const updateProduct = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { name, category_id, reference, price, stock } = req.body;

    const result = await pool.query(
      "UPDATE konecta_inventory.product SET name = $1, category_id = $2, reference = $3, price = $4, stock = $5 WHERE id = $6 RETURNING *",
      [name, category_id, reference, price, stock, id]
    );

    if (result.rows.length === 0)
      return res.status(404).json({ message: "Product not found" });

    return res.json(result.rows[0]);
  } catch (error) {
    next(error);
  }
};

const deleteProduct = async (req, res, next) => {
  try {
    const { products } = req.body;

    products.map( async (product) =>{
      const result = await pool.query(
        "UPDATE konecta_inventory.product SET active = 0 WHERE id = $1",
        [product]
      );  
      
      if (result.rowCount === 0)
        return res.status(404).json({ message: "Product not found" });
    });
    
    return res.sendStatus(200);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  createProduct,
  getAllProduct,
  getProduct,
  updateProduct,
  deleteProduct,
};

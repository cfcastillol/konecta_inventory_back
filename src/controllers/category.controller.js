const pool = require("../db");

const createCategory = async (req, res, next) => {
  try {
    const { category } = req.body;

    const newCategory = await pool.query(
      "INSERT INTO konecta_inventory.category (category) VALUES($1) RETURNING *",
      [category]
    );

    res.json(newCategory.rows[0]);
  } catch (error) {
    next(error);
  }
};

const getAllCategory = async (req, res, next) => {
  try {
    const allCategorys = await pool.query("SELECT * FROM konecta_inventory.category");
    res.json(allCategorys.rows);
  } catch (error) {
    next(error);
  }
};

const getCategory = async (req, res, next) => {
  try {
    const { id } = req.params;
    const result = await pool.query("SELECT * FROM konecta_inventory.category WHERE id = $1", [id]);

    if (result.rows.length === 0)
      return res.status(404).json({ message: "Category not found" });

    res.json(result.rows[0]);
  } catch (error) {
    next(error);
  }
};

const updateCategory = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { category } = req.body;

    const result = await pool.query(
      "UPDATE konecta_inventory.category SET category = $1, WHERE id = $2 RETURNING *",
      [category, id]
    );

    if (result.rows.length === 0)
      return res.status(404).json({ message: "Category not found" });

    return res.json(result.rows[0]);
  } catch (error) {
    next(error);
  }
};

const deleteCategory = async (req, res) => {
  try {
    const { id } = req.params;
    const result = await pool.query("DELETE FROM konecta_inventory.category WHERE id = $1", [id]);

    if (result.rowCount === 0)
      return res.status(404).json({ message: "Category not found" });
    return res.sendStatus(204);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  createCategory,
  getAllCategory,
  getCategory,
  updateCategory,
  deleteCategory,
};

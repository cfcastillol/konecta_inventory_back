const pool = require("../db");

const discountProduct = (product) => {

  const {id, qty, stock} = product;
  const newQty = stock - qty

  return pool.query(
    "UPDATE konecta_inventory.product SET stock = $1 WHERE id = $2",
    [newQty, id]
  );


} 

const createSale = async (req, res, next) => {
  try {
    const { sale, products } = req.body;
  
    //console.log(sale, products);

    const newSale = await pool.query(
      "INSERT INTO konecta_inventory.sale (total_sale) VALUES($1) RETURNING *",
      [sale]
    );

    const saleId = newSale.rows[0].id;
    let values = '';
    const query = "INSERT INTO konecta_inventory.product_sale (product_id, sale_id, product_qty, product_name, product_reference, product_price, product_category, product_stock) VALUES ";


    products.map( async(product) =>{
      values = values+`(${product.id},${saleId},${product.qty},'${product.name}','${product.reference}',${product.price},'${product.category}',${product.stock}),`;
      await discountProduct(product);
    });

    const fullQuery = query + values.substring(0, values.length - 1);

    const newProuctSale = await pool.query(
      fullQuery
    );

    res.json(newSale.rows[0]);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  createSale
};

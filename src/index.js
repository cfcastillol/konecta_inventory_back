const express = require("express");
const cors = require("cors");
const morgan = require("morgan");
const category = require("./routes/category.routes");
const product = require("./routes/product.routes");
const sale = require("./routes/sales.routes");

const app = express();

// Settings
app.set("port", process.env.PORT);

// Middlewares
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Routes
app.use(category, product, sale);

// handling errors
app.use((err, req, res, next) => {
  return res.status(500).json({
    status: "error",
    message: err.message,
  });
});

app.listen(app.get("port"));
console.log("Server on port", app.get("port"));
